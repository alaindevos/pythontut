#!/usr/bin/python3.11
import lupa
from lupa import LuaRuntime
lua = LuaRuntime(unpack_returned_tuples=True)
a=lua.eval('1+1')
print(a)

lua_func = lua.eval('function(f, n) return f(n) end')
def py_add1(n): 
    return n+1
b=lua_func(py_add1, 2)
print(b)

c=lua.eval('python.eval(" 2 ** 2 ")')
print(c)

d=lua.eval('python.builtins.str(4)')
print(d)

