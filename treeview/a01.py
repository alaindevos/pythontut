#!/usr/bin/python3.11
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk



class PopUp(Gtk.Dialog):
    def __init__(self,parent):
        Gtk.Dialog.__init__(self,"MyPopUpTitle",parent,Gtk.DialogFlags.MODAL)
        self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.set_default_size(200,200)
        self.set_border_width(30)
        area=self.get_content_area()
        area.add(Gtk.Label("WOW"))
        self.show_all()
        
    


class MyWindow(Gtk.Window):

    people=[("Alain","Devos"),
            ("Eddy","DeWolf")]

    def __init__(self):
        Gtk.Window.__init__(self,title="MYWINDOW")
        self.set_border_width(10)
        self.set_size_request(600,400)
        self.connect("destroy", Gtk.main_quit)

        self.box=Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=10)

        self.button1=Gtk.Button(label="ClickMe")
        self.button1.connect("clicked",self.button_clicked)
        self.box.pack_start(self.button1,True,True,0)

        self.label1=Gtk.Label()
        self.label1.set_markup("<small> Small text </small> <big> Big text </big> <b> Bold </b>  <i> Italics </i> \n")
        self.label1.set_halign(Gtk.Align.END)
        self.label1.set_justify(Gtk.Justification.FILL)
        self.label1.set_line_wrap(True)
        self.box.pack_start(self.label1,True,True,0)

        self.label2=Gtk.Label()
        self.label2.set_markup("*****************************************")
        self.label2.set_angle(30)
        self.label2.set_halign(Gtk.Align.END)
        self.label2.set_justify(Gtk.Justification.FILL)
        self.label2.set_line_wrap(True)
        print(self.label2.get_properties("angle"))
        self.box.pack_start(self.label2,True,True,0)
        
        self.username=Gtk.Entry()
        self.username.set_text("Username")
        self.box.pack_start(self.username,True,True,0)
        
        self.people_list_store=Gtk.ListStore(str,str)
        for item in self.people:
            self.people_list_store.append(list(item))
            
        self.people_tree_view=Gtk.TreeView(self.people_list_store)
        for i,col_title in enumerate(["Firstname","Lastname"]):
            renderer=Gtk.CellRendererText()
            column=Gtk.TreeViewColumn(col_title,renderer,text=i)
            column.set_sort_column_id(i)
            self.people_tree_view.append_column(column)
        selected_row=self.people_tree_view.get_selection()
        selected_row.connect("changed",self.item_selected)
        self.box.pack_start(self.people_tree_view,True,True,0)
        
        self.button2=Gtk.Button("Open a Popup")
        self.button2.connect("clicked",self.popupclicked)
        self.box.pack_start(self.button2,True,True,0)
        
        
        self.add(self.box)
        
    def item_selected(self,selection):
        model,row=selection.get_selected()
        if row is not None:
            print(model[row][0]+":"+model[row][1])

    def button_clicked(self,widget):
        print("Button Clicked")
        print(widget.get_properties("label"))
        print(self.username.get_text())

    def popupclicked(self,widget):
        dialog=PopUp(self)
        response=dialog.run()
        if response==Gtk.ResponseType.OK:
            print("You clicked OK")
        elif response==Gtk.ResponseType.CANCEL:
            print("You clicked CANCEL")
        dialog.destroy()
        
        
window = MyWindow()
window.show_all()
Gtk.main()
