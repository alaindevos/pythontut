#!/usr/local/bin/python3.9

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

#List of tuples
data=[("Alain",17) , ("Eddy",18) , ("Jan",19)]

class MainWindow(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self,title="MyTreeView")
		layout=Gtk.Box()
		self.add(layout)
		myliststore=Gtk.ListStore(str,int)
		for row in data:
			myliststore.append(list(row))
		for row in data:
			print(row[:])
		mytreeview=Gtk.TreeView(model=myliststore)
		for t, coltitle in enumerate(["Name","Age"]):
			renderer=Gtk.CellRendererText()
			col=Gtk.TreeViewColumn(coltitle,renderer,text=t)
			#Make columns sortable
			col.set_sort_column_id(t)
			#Add column to treeview
			mytreeview.append_column(col)
		myrowselection=mytreeview.get_selection()
		myrowselection.connect("changed",self.rowselected)
		layout.pack_start(mytreeview,True,True,0)

	def rowselected(self,selection):
		model,row=selection.get_selected()
		print("Name:"+model[row][0])
		index = selection.get_selected_rows()[1][0][0]
		print(index)

window=MainWindow()
window.connect("delete-event",Gtk.main_quit)
window.show_all()
Gtk.main()
