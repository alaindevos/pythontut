#!/usr/bin/python3.11

import npyscreen

class MyForm(npyscreen.ActionForm,npyscreen.SplitForm,npyscreen.FormWithMenus):

    def afterEditing(self):
        pass

    def apress(self):
        npyscreen.notify_confirm ("AAA","BBB",editw=1)
    def exit_form(self):
        self.parentApp.switchForm(None)

    def on_ok(self):
        self.firstname.value="Pressed OK"
        npyscreen.notify_ok_cancel("AAA","BBB")
    def on_cancel(self):
        self.lastname.value="Pressed Cancel"
        self.parentApp.setNextForm(None)

    def create(self):
        self.show_atx=0
        self.show_aty=5

        self.firstname=self.add(npyscreen.TitleText,name='Firstname:',value='enter')
        self.nextrely += 1

        self.lastname=self.add(npyscreen.TitleText,name=' Lastname:',value='enter')

        self.menu=self.new_menu("Main Menu",shortcut='m')
        self.menu.addItem("Item 1 ",self.apress,"1")
        self.menu.addItem("Item 2 ",self.apress,"2")
        self.menu.addItem("Exit Form",self.exit_form,"^x")

class MyApp(npyscreen.NPSAppManaged):
    def onStart(self):
        self.addForm('MAIN',MyForm,name="MyFormtitle",lines=10)

if  (__name__=="__main__"):
    app=MyApp()
    app.run()
    print("The End")
