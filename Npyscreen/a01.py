#!/usr/local/bin/python3.9

import npyscreen

def simple_function(*arguments):
    form=npyscreen.Form(name='MyTitle')
    form.display()
    form.edit()


if  (__name__=="__main__"):
    npyscreen.wrapper_basic(simple_function)
    print ("The End")
