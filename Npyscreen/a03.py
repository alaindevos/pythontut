#!/usr/local/bin/python3.9

import npyscreen

class MyForm(npyscreen.Form):
    def create(self):
        pass

class MyApp(npyscreen.NPSAppManaged):
    def onStart(self):
        self.addForm('MAIN',MyForm,name="MyFormtitle")

if  (__name__=="__main__"):
    app=MyApp()
    app.run()
    print("The End")
